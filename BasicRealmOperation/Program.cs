﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Realms;
using Realms.Sync;

namespace CreationLine.SampleApp.BasicRealmOperation
{
    class Program
    {
        /// <summary>
        /// Api Key を取得するための user-secrets のキー名。
        /// </summary>
        const string SECRET_APIKEY = "MongoDB:Realm:ApiKey";
        
        /// <summary>
        /// App ID を取得するための user-secrets のキー名。
        /// </summary>
        const string SECRET_APPID = "MongoDB:Realm:AppId";
        
        /// <summary>
        /// PUBLIC のパーティション名
        /// </summary>
        const string PUBLIC_PARTITION = "PUBLIC";
        
        static readonly string s_realmDir = Path.GetDirectoryName(
            Assembly.GetExecutingAssembly().Location) + "/../../../Realms/";

        static async Task Main(string[] args)
        {
            // user-secrets から設定値を読み込めるようにします。
            // - config["user-secrets のキー名"] で設定値を取得できます。
            IConfiguration config = new ConfigurationBuilder()
                .AddUserSecrets<Program>().Build();

            // Realm DB を配置する為のフォルダを作成します。
            Directory.CreateDirectory(s_realmDir);

            // Realm アプリに Api Key で接続 (ログイン) します。
            App app = App.Create(config[SECRET_APPID]);
            User user = await app.LogInAsync(Credentials.ApiKey(config[SECRET_APIKEY]));
            
            // Realm DB "user.realm" に接続します。パーティション "User ID の値" に属します。
            // - SyncConfiguration が Realm アプリとの同期設定になります。
            // - この設定を追加しておけば、後は Realm DB の操作をするだけで、
            //   MongoDB Atlas にデータが同期されるようになります。
            // - つまり、クラウド上の MongoDB Atlas を直接操作する必要はありません！
            //   ローカルの Realm DB だけ意識すれば良いということになります！！
            using Realm userRealm = Realm.GetInstance(
                new SyncConfiguration(user.Id, user, s_realmDir + "user.realm"));
 
            // Realm DB "public.realm" に接続します。パーティション "PUBLIC" に属します。
            using Realm publicRealm = Realm.GetInstance(
                new SyncConfiguration(PUBLIC_PARTITION, user, s_realmDir + "public.realm"));

            Thread.Sleep(2000);

            while (true)
            {
                Console.Write("[A]dd, [M]odify, [R]emoveAll, [U]serData, [P]ublicData, [Q]uit: ");
                ConsoleKeyInfo keyInfo = Console.ReadKey();
                
                Console.WriteLine();
                userRealm.Refresh();
                publicRealm.Refresh();

                switch (keyInfo.Key)
                {
                //----------------------------------------------------------------------------
                // [ A ]
                // 各パーティションの、新しいデータを追加します。
                // - Realm DB のデータ更新のお作法として、C# オブジェクトでの Add や Remove などの
                //   操作 (Action) を、Realm.Write() を実行することにより、実際の Realm DB に
                //   書き込みます。
                // - Write() は Action に対するトランザクションの管理をしており、
                //   例外がスローされなければコミットします。
                // - このサンプルプログラムの場合は、SyncConfiguration が設定されているので、
                //   クラウドの MongoDB Atlas にも書き込み (同期) が行われます。
                //----------------------------------------------------------------------------
                case ConsoleKey.A:
                    userRealm.Write(() => Console.WriteLine(
                        userRealm.Add(Computer.Create(user.Id, "DesktopPC", "Me"))));
                    publicRealm.Write(() => Console.WriteLine(
                        publicRealm.Add(Computer.Create(PUBLIC_PARTITION, "LaptopPC", "Share"))));
                    Console.WriteLine("Added new documents.");
                    break;

                //----------------------------------------------------------------------------
                // [ M ]
                // 各パーティションの一番新しいデータを変更します。
                // "(Modified)" というテキストを付加するだけです。
                //----------------------------------------------------------------------------
                case ConsoleKey.M:
                    Computer com;
                    if ((com = userRealm.All<Computer>().LastOrDefault()) != null)
                    {
                        userRealm.Write(() => com.Name += "(Modified)");
                        Console.WriteLine("Modified the last user document.");
                    }
                    if ((com = publicRealm.All<Computer>().LastOrDefault()) != null)
                    {
                        publicRealm.Write(() => com.Name += "(Modified)");
                        Console.WriteLine("Modified the last public document.");
                    }
                    break;
                
                //----------------------------------------------------------------------------
                // [ R ]
                // 全てのデータを削除します。
                //----------------------------------------------------------------------------
                case ConsoleKey.R:
                    userRealm.Write(() => userRealm.RemoveAll());
                    publicRealm.Write(() => publicRealm.RemoveAll());
                    Console.WriteLine("Removed all documents.");
                    break;

                //----------------------------------------------------------------------------
                // [ U ]
                // パーティション名 "User ID の値" に属するデータをリストします。
                //----------------------------------------------------------------------------
                case ConsoleKey.U:
                    userRealm.All<Computer>().ToList().ForEach(c => Console.WriteLine(c.ToString()));
                    break;

                //----------------------------------------------------------------------------
                // [ P ]
                // パーティション名 "PUBLIC" に属するデータをリストします。
                //----------------------------------------------------------------------------
                case ConsoleKey.P:
                    publicRealm.All<Computer>().ToList().ForEach(c => Console.WriteLine(c.ToString()));
                    break;

                //----------------------------------------------------------------------------
                // [ Q ]
                // アプリを終了します。
                //----------------------------------------------------------------------------
                case ConsoleKey.Q:
                    await user.LogOutAsync();
                    return;

                default:
                    continue;
                }
            }
        }
    }
}